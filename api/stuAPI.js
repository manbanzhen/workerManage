export function addStudent(name, classes, stuNumber) {
    let params = new FormData();
    params.append("name", name)
    params.append("classes", classes)
    params.append("stuNumber", stuNumber)
    return{
        url: `/student/`,
        method: 'post',
        data: params
    }
}


export function getAllStudent() {
    return{
        url: `/student`,
        method: 'get'
    }
}

export function getAllSub() {
    return{
        url: `/get_subject/`,
        method: 'get'
    }
}


export function addResult(stu_id, sub_id, usual, final, total) {
    let params = new FormData();
    params.append("stu_id", stu_id)
    params.append("sub_id", sub_id)
    params.append("usual", usual)
    params.append("final", final)
    params.append("total", total)
    return{
        url: `/result/`,
        method: 'post',
        data: params
    }
}


export function getStudentResults(stu_id) {
    return{
        url: `/get_one_student/?stu_id=${stu_id}`,
        method: 'get'
    }
}

export function deleteResult(stu_id, sub_id) {
    return{
        url: `/del_subject/?stu_id=${stu_id}&sub_id=${sub_id}`,
        method: 'get'
    }
}

export function getResult(stu_id, sub_id) {
    return{
        url: `/result/?stu_id=${stu_id}&sub_id=${sub_id}`,
        method: 'get'
    }
}

export function updateResult(stu_id, sub_id, usual, final, total) {
    let params = new FormData();
    params.append('stu_id', stu_id)
    params.append('sub_id', sub_id)
    params.append('usual', usual)
    params.append('final', final)
    params.append('total', total)
    return{
        url: `/update_result/`,
        method: 'post',
        data: params
    }
}


export function delStudent(stu_id) {
    return{
        url: `/del_student/?stu_id=${stu_id}`,
        method: 'get'
    }
}


export function searchStu(query) {
    return{
        url: `/search_student/?query=${query}`,
        method: 'get'
    }
}

